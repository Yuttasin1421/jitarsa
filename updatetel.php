<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}
.flexbox .col-sm-2{
    padding-left: 0;
    padding-right:0;
    }
.block {
    width: 30%;
    background: #fff;
}
.button {
  background-color: #4CAF50; /* Green */
  border: none;
  color: green;
  /*padding: 4px 4px;*/
  /*text-align: center;*/
  border-radius: 10px;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 0px 0px;
  /*transition-duration: 0.4s;*/
  /*cursor: pointer;*/
  box-shadow: 1px 1px 1px #888888;
  float: left;

}
.button1 {
  background-color: white; 
  color: black; 
  border: 2px solid #4CAF50;
  height:30px;
  width:100px;
}
.button2 {
  background-color: white; 
  color: black; 
  border: 2px solid red;
  height:30px;
  width:100px;
  float: right;
}

</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
 <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
   <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script> 
  
<?php
	require('connect.php');
	if($_SESSION["student_id"]==""){
		header("Location: studentlogin.php");
	}else{
        $student_id = $_SESSION["student_id"];
        if(isset($_POST['tel'])){
            $tel = mysqli_real_escape_string($conn,$_POST['tel']);
            $query = "UPDATE Student Set tel = '$tel' WHERE student_id ='$student_id'";
            $result = mysqli_query($conn,$query);
            echo '<script>window.location.href="stu_profile.php";</script>';
        }

        $student_id=$_SESSION["student_id"];
        $query = "SELECT *,Teacher.prefix as tprefix,Teacher.firstname as tfirstname,Teacher.lastname as tlastname,Student.tel as stel,Student.photo as sp,Student.prefix as sprefix,Student.firstname as sfirstname,Student.lastname as slastname From Student INNER JOIN Room ON student.classroom=room.classroom JOIN Teacher ON room.teacher_id=teacher.teacher_id WHERE student.student_id='$student_id' ORDER BY student_id DESC";
        $result = mysqli_query($conn,$query);
        //$row = mysqli_fetch_assoc($result);
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
				<p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:pink">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
		<div class="col-sm-3" align="center"><p><a href=index.php>หน้าหลัก</a></p></div>
		<div class="col-sm-3" align="center"><p><a href=stu_volunteerinfo.php>ข้อมูลจิตอาสา</a></p></div>
		<div class="col-sm-3" align="center"><p style="color:brown"><b><u>ข้อมูลส่วนตัว</u></b></p></div>
		<div class="col-sm-3" align="center"><p><a href=logout.php>ออกจากระบบ</a></p></div>
	</div>
</div>
</div>
</nav>  
<div class="content">
    <br>
    <div class="row">
        <div class="col-sm-3 " align="center"></div>
        <div class="col-sm-2" align="center"><img src="photo/<?php echo $student_id;?>.jpeg" class="rounded-circle";" style="width:200px%; height:200px" alt="Image"></div>
        <div class="col-sm-4 " align="left">
        <?php 
        $row = mysqli_fetch_assoc($result);
        echo "<br>";
        echo '<p style="" align="left"><b><u>ข้อมูลส่วนตัว</u><b></p>
        <p>';echo 'ชื่อ '.$row['sprefix'].' '.$row['sfirstname'].' '.$row['slastname'];echo'</p>
        <p>';echo 'รหัสนักเรียน '.$row['student_id'];echo'</p>
        <p>';?>
        <form form method="POST" action="updatetel.php"><label for="tel" class=""align="left"><b>เบอร์โทรศัพท์</b></label>
        <input type="text" name="tel" id="tel" placeholder="<?php echo $row['stel']; ?>" style="border-radius: 25px;" value="<?php echo $row['stel'];?>"required>
        <button type="submit" class="btn btn-primary" name="submit">ยืนยัน</button>    
    </form>
        
        <p><?php ;echo 'ครูประจำชั้น '.$row['tprefix'].' '.$row['tfirstname'].' '.$row['tlastname'];echo'</p>
        <p></p>';?>
        </div>
        <div class="col-sm-3 " align="center"></div>
        </div>
        <br>
        <div class="row">
            <div class="col-sm-2"></div>
            <div class="col-sm"><p><u>บันทึกกิจกรรมอาสา</u></p></div>
                    </div>
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-8">
<table class="table table-bordered"style="text-align:center" align="center">
  <thead>
    <tr class="table-warning" >
        <td scope="col">ที่</td>
        <td scope="col">วัน/เดือน/ปี</td>
        <td scope="col">สถานที่</td>
        <td scope="col">ชื่อกิจกรรม</td>
        <td scope="col">จำนวนชม</td>
        <td scope="col">ผลการอนุมัติ</td>
        <td scope="col">หมายเหตุ</td>
    </tr>
  </thead><?php
        //$query2 ="SELECT * FROM volunteer JOIN student ON volunteer.student_id = student.student_id WHERE student.student_id=$student_id";
        $query2 ="SELECT * FROM volunteer JOIN student ON volunteer.student_id = student.student_id WHERE Student.student_id='$student_id' ORDER BY create_at DESC";
        $result = mysqli_query($conn,$query2);
        $count = 1;
        $hour=0;
    while($row2=mysqli_fetch_assoc($result)){
        echo'<tbody>
            <tr class="table-light">
                <td>';echo $count++;echo'
                <td>';echo $row2['date'];echo'</th>
                <td>';echo $row2['location'];echo'</td>
                <td>';echo $row2['Volunteer_name'];echo'</td>
                <td>';echo $row2['Hours'];echo'</td>
                <td>';echo $row2['isapprove'];echo'</td>
                <td>';echo $row2['annotate'];echo'</td>
            </tr>
        </tbody>';
                if($row2['isapprove']=="อนุมัติ"){
                $hour+=$row2['Hours'];
                }
        }?>
        <tbody>
            <tr class="table-light">
                <td colspan="4">จำนวนชม</td>
                <td colspan="3"><?php echo $hour;?> Hr. </td>
    </tbody>

</table>
        </div>
        <div class="col-sm-2"></div>
    </div>
        </div>

<?php } ?>
</body>
</html>