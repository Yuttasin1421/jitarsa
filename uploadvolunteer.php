<!DOCTYPE html>
<html>
  <head>
   <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
   <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
   <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

  </head>
  <body>
<?php
//session_start();
require('connect.php');
error_reporting(0);
  $msg = "";
  if (isset($_POST['upload'])) {
    //$student_id = $_SESSION["student_id"];
    $student_id = mysqli_real_escape_string($conn,$_POST['student_id']);
    $query = "SELECT * FROM Student JOIN Room ON Student.classroom = Room.classroom WHERE student_id ='$student_id'";
    $result = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($result);
    $teacher_id = $row['teacher_id'];
    //echo $row['teacher_id'];
    $Volunteer_name = mysqli_real_escape_string($conn,$_POST['Volunteer_name']);
    $Hours =  mysqli_real_escape_string($conn,$_POST['Hours']);
    $location = mysqli_real_escape_string($conn,$_POST['location']);
    $amphur = mysqli_real_escape_string($conn,$_POST['amphur']);
    $distinct = mysqli_real_escape_string($conn,$_POST['distinct']);
    $province = mysqli_real_escape_string($conn,$_POST['province']);
    $date = mysqli_real_escape_string($conn,$_POST['date']);
    $semester = mysqli_real_escape_string($conn,$_POST['semester']);
    $witness = mysqli_real_escape_string($conn,$_POST['witness']);
    $witness_email = mysqli_real_escape_string($conn,$_POST['email']);
    $description = mysqli_real_escape_string($conn,$_POST['description']);
    $filename = $_FILES["photo"]["name"];
    $tempname = $_FILES["photo"]["tmp_name"];    
    $folder = "photo/".$filename;
    $query2 = "SELECT Count(*) as total FROM Volunteer";
    $data = mysqli_query($conn,$query2);
	$ans = mysqli_fetch_assoc($data);
    $volunteer_id =$ans['total']+1;
        if (move_uploaded_file($tempname, $folder))  {
            $today = date("Y-m-d");
            $query = "INSERT INTO Volunteer(volunteer_id, student_id, teacher_id, date,create_at,description, annotate, isapprove, witness_name, witness_email, photo, semester, location, Volunteer_name, Hours,amphur,distincts,province) 
            VALUES ('$volunteer_id','$student_id','$teacher_id','$date','$today','$description','','รอดำเนินการ','$witness','$witness_email','photo/$filename','$semester','$location,',
            '$Volunteer_name','$Hours','$amphur','$distinct','$province')";
            $result = mysqli_query($conn,$query);
            ?> 
            <script> 
            swal({
        title: "สำเร็จ!",
        text: "ส่งคำร้องจิตอาสาสำเร็จ",
        type: "success",
        }).then(function(){
            window.location.href = "stu_profile.php";
        });
        </script>'
            <?php
        }
        else{?>
            <script>
            swal({
                title: "ล้มเหลว!",
                text: "การเพิ่มจิตอาสาล้มเหลว",
                type: "error",
                }).then(function(){
                    window.location.href = "stu_volunteerinfo.php";
                });
                </script>
                <?php
      }
  }
?>
</body>
</html>