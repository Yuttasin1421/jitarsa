<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}

</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php
	require('connect.php');
	if(isset($_GET['volunteer'])){
    $teacher_id = $_SESSION["teacher_id"];
    $volunteer_id = mysqli_real_escape_string($conn,$_GET['volunteer']);
    $query = "SELECT *,Volunteer.photo as vp FROM Volunteer JOIN Student On Student.student_id = Volunteer.student_id WHERE Volunteer_id = '$volunteer_id'";
    $result = mysqli_query($conn,$query);
    $row = mysqli_fetch_assoc($result);
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
        <p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:#006400">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
		<div class="col-sm" align="center"><p><a href=teacher_index.php>หน้าหลัก</a></p></div>
        <div class="col-sm" align="center"><p style="color:brown"><b><u>การอนุมัติจิตอาสา</u></b></p></div>
        <div class="col-sm" align="center"><p><a href=teacher_profile.php>ข้อมูลส่วนตัว</a></p></div>
    <div class="col-sm" align="center"><p><a href=logout.php name="logout" value="logout" id="logout">ออกจากระบบ</a></p></</div>
	</div>
</div>
</div>
</nav>
<br>
<div class="content">
    <div class="row">
		<div class="col-sm-3" style="text-align:center">
			รายละเอียดคำร้องขอชั่วโมงจิตอาสา
		</div>
		<div class="col-sm-9"></div>
	</div>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
        <div class="panel panel-default">
            <div class="panel-heading">ชื่อกิจกรรม : <?php echo $row['Volunteer_name'] ?></div>
            <div class="panel-body"><img src="<?php echo $row['vp'];?>" style="max-width:500px;width:100%">
            <div class="panel-body">วันที่ยื่นคำขอ <?php echo $row['create_at'];?></div>
            <div class="panel-body">วันทีทำจิตอาสา <?php echo $row['date'];?></div>
            <div class="panel-body">จำนวนชั่วโมง <?php echo $row['Hours'];?> ชั่วโมง</div>
            <div class="panel-body">สถานที่ <?php echo $row['location'];?></div>
            <div class="panel-body">อำเภอ <?php echo $row['amphur'];?> </div>
            <div class="panel-body">ตำบล <?php echo $row['distincts'];?> </div>
            <div class="panel-body">จังหวัด <?php echo $row['province'];?> </div>
            <div class="panel-body">ชื่อผู้รับรอง <?php echo $row['witness_name'];?> </div>
            <div class="panel-body">email ผู้รับรอง <?php echo $row['witness_email'];?> </div>
            <div class="panel-footer">ผู้ยื่นคำขอ <?php echo $row['prefix'].' '.$row['firstname'].' '.$row['lastname']; ?> รหัสนักเรียน <?php echo $row['student_id'] ?></div>
  </div>
		</div>
		<div class="col-sm-2"></div>
	</div>
    </div>
	<?php } ?>
</body>
</html>