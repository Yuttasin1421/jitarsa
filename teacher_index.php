<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}
</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type=text/javascript src="jquery.min.js"></script>
<script src="jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>


  
<?php
	require('connect.php');
	if($_SESSION["teacher_id"]==""){
		header("Location: teacherlogin.php");
	}else{
		$teacher_id=$_SESSION["teacher_id"];
		if (isset($_POST['liked'])) {
			echo "cannot add";
			$announce_id = $_POST['announce_id'];
			$result = mysqli_query($conn, "SELECT * FROM Announce WHERE announce_id=$announce_id");
			$row = mysqli_fetch_array($result);
			//count like
			$liked = mysqli_query($conn,"SELECT Count(*) as totallike FROM Announce WHERE 1");
			$r = mysqli_fetch_assoc($liked);
			$n = $r['totallike'];
			mysqli_query($conn, "INSERT INTO liked (teacher_id, announce_id) VALUES (1, $announce_id)");
			mysqli_query($conn, "UPDATE Announce SET liked=$n+1 WHERE announce_id=$announce_id");
			$n+=1;
			exit();
		}
		if (isset($_POST['unliked'])) {
			$announce_id = $_POST['announce_id'];
			$liked = mysqli_query($conn,"SELECT Count(*) as totallike FROM Announce WHERE 1");
			$r = mysqli_fetch_assoc($liked);
			$n = $r['totallike'];
			mysqli_query($conn, "DELETE FROM liked WHERE announce_id=$announce_id AND teacher_id=$teacher_id");
			$n-=1;
			exit();
		}
	
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
				<p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:#006400">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
		<div class="col-sm" align="center"><p style="color:brown"><b><u>หน้าหลัก</u></b></p></div>
		<div class="col-sm" align="center"><p><a href=teacher_volunteerapprove.php>การอนุมัติจิตอาสา</a></p></div>
		<div class="col-sm" align="center"><p><a href=teacher_profile.php>ข้อมูลส่วนตัว</a></p></div>
		<div class="col-sm" align="center"><p><a href=logout.php name="logout" value="logout" id="logout">ออกจากระบบ</a></p></div>
	</div>
</div>
</div>
</nav>

<div class="content">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm">
            <br>
             <button onclick="location.href = 'create_announce.php';" id="myButton" class="float-left btn btn-success btn-lg " >สร้างประกาศใหม่</button>
            <br><br>
			<?php
			$query = "SELECT *,Announce.photo as ap,CAST(Announce.announce_id AS UNSIGNED) AS id From announce INNER JOIN Teacher ON announce.teacher_id = teacher.teacher_id ORDER BY id DESC";
			$result = mysqli_query($conn,$query);
			while($row =mysqli_fetch_assoc($result)){
				$type = 0;
				$query2 = "SELECT COUNT(*) as total FROM liked WHERE announce_id ='$row[announce_id]'";
				$data = mysqli_query($conn,$query2);
				$ans = mysqli_fetch_assoc($data);
				$status_query = "SELECT COUNT(*) as is_like FROM liked WHERE  announce_id ='$row[announce_id]'";
				$status_result = mysqli_query($conn,$status_query);
				$status_row = mysqli_fetch_array($status_result);
				$count_status = $status_row['is_like'];
				if($count_status>0){
					$type = 1;
				}?>
				<div class="post"><?php
				$results = mysqli_query($conn, "SELECT * FROM liked WHERE   announce_id=".$row['announce_id']."");
				if (mysqli_num_rows($results) == 1 ): 
					
				echo '<br>';
				echo '<div class="panel panel-success">
					<div class="panel-heading">';echo 'ชื่อกิจกรรม '.$row['announce_name'];echo '</div>
					<div class="panel-body"><img src="';echo $row['ap'];echo '" class="img-thumbnail;" style="width:20%;" alt="Image"></div>
					<div class="panel-body">';echo 'รายละเอียด: '.$row['announce_description']; echo '</div>
					<div class="panel-body">';echo 'สถานที่: '.$row['location']; echo '</div>
					<div class="panel-footer">';
				echo ' '.$ans['total'].' คนถูกใจสิ่งนี้';echo'</div></div>';
				 else: 
				echo '<br>';
				echo '<div class="panel panel-success">
					<div class="panel-heading">';echo 'ชื่อกิจกรรม '.$row['announce_name'];echo '</div>
					<div class="panel-body"><img src="';echo $row['ap'];echo '" class="img-thumbnail;" style="width:20%;" alt="Image"></div>
					<div class="panel-body">';echo 'รายละเอียด: '.$row['announce_description']; echo '</div>
					<div class="panel-body">';echo 'สถานที่: '.$row['location']; echo '</div>
					<div class="panel-footer">';
					echo ' '.$ans['total'].' คนถูกใจสิ่งนี้';echo'</div></div>';?>
				<?php endif;?>
				</div><?php
			  }
			?>
			</div>
		<div class="col-sm-2"></div>
	</div>	
</div>
<?php } ?>
<script type="text/javascript"  
    src="http://code.jquery.com/jquery.min.js" 
    src="jquery.min.js"
    src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"
></script>
<script type="text/javascript" src="jquery.min.js"></script>

<script>
$document.addEventListener("DOMContentLoaded", function(){
  window.addEventListener('scroll', function() {
      if (window.scrollY > 50) {
        document.getElementById('navbar_top').classList.add('fixed-top');
        // add padding top to show content behind navbar
        navbar_height = document.querySelector('.navbar').offsetHeight;
        document.body.style.paddingTop = navbar_height + 'px';
      } else {
        document.getElementById('navbar_top').classList.remove('fixed-top');
         // remove padding top from body
        document.body.style.paddingTop = '0';
      } 
  });
}); 
</script>
</body>
</html>