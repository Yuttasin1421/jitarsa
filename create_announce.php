<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}

</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php
	require('connect.php');
	if($_SESSION["teacher_id"]==""){
		header("Location: studentlogin.php");
	}else{
    $teacher_id = $_SESSION["teacher_id"];
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
        <p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:#006400">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
	<div class="col-sm" align="center"><p style="color:brown"><b><u>หน้าหลัก</u></b></p></div>
		<div class="col-sm" align="center"><p><a href=teacher_volunteerapprove.php>การอนุมัติจิตอาสา</a></p></div>
		<div class="col-sm" align="center"><p><a href=teacher_profile.php>ข้อมูลส่วนตัว</a></p></div>
		<div class="col-sm" align="center"><p><a href=logout.php name="logout" value="logout" id="logout">ออกจากระบบ</a></p></div>
	</div>
</div>
</div>
</nav>
<br>
<?php include('uploadannounce.php') ?>
<div class="content">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-6">
		<form method="POST" action="uploadannounce.php" enctype="multipart/form-data">
  <div class="form-group">
    <label for="announce_name">ชื่อประกาศ</label>
    <input type="text" class="form-control" name="announce_name" id="announce_name" aria-describedby="announce_name" placeholder="ใส่ชื่อประกาศ">
    <small id="text" name="text" class="form-text text-muted">*ชื่อประกาศไม่สามารถว่างเปล่าได้</small>
  </div>
  <div class="form-group">
    <label for="สถานที่">สถานที่</label>
    <input type="text" class="form-control" name="location" id="location" placeholder="สถานที่">
  </div>
  <div class="form-group">
  		<label for="photo" class="col-md-2 col-form-label" align="left"><b>แนบไฟล์รูป</b></label>
        <div class="col-md-10">
        <input type="file" name="photo" id="photo"placeholder="ไฟล์รูป" style="border-radius: 25px;" value=""/ required>
		</div>
		<br>
  </div>
  <div class="row">
        <label for="description" class="col-md-20 col-form-label" align="left"><b>รายละเอียด</b></label>
        <div class="col-md-10">
        <textarea class="form-control" name="announce_description" id="announce_description" placeholder="รายละเอียด" required></textarea>
    </div>
    </div><br>
	<div class="row">
        <input type="hidden" name="teacher_id" id="hiddenField" value="<?php echo $teacher_id; ?>" />
        <div>
          <button type="submit"  class="btn btn-primary"name="upload" align="center">ยืนยัน</button>
        </div>
        </div>
</form>

		</div>
		<div class="col-sm-2"></div>
	</div>
    
    </div>
<?php } ?>
</body>
</html>