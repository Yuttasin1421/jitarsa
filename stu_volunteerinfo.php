<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}

</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php
	require('connect.php');
	if($_SESSION["student_id"]==""){
		header("Location: studentlogin.php");
	}else{
    $student_id = $_SESSION["student_id"];
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
				<p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:pink">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
		<div class="col-sm" align="center"><p><a href=index.php>หน้าหลัก</a></p></div>
        <div class="col-sm" align="center"><p style="color:brown"><b><u>การอนุมัติจิตอาสา</u></b></p></div>
		<div class="col-sm" align="center"><p><a href=stu_profile.php>ข้อมูลส่วนตัว</a></p></div>
    <div class="col-sm" align="center"><p><a href=logout.php name="logout" value="logout" id="logout">ออกจากระบบ</a></p></</div>
	</div>
</div>
</div>
</nav>
<br>
<?php include('uploadvolunteer.php') ?>
<div class="content">
    <div class="row">
        <div class="col-sm-2"></div>
        <div class="col-sm-2"><p><u>คำร้องขอชั่วโมงจิตอาสา</u></p></div>
        <div class="col-sm-8"></div>
    </div>
    <br>
    <div class="row">
        <div class="col-sm-3"></div>
        <div class="col-sm-6">
    <form method="POST" action="uploadvolunteer.php" enctype="multipart/form-data">
        <div class="row g-3">
        <label for="Volunteer_name" class="col-md-2 col-form-label" align="left"><b>ชื่อกิจกรรม</b></label>
        <input type="text" name="Volunteer_name" id="Volunteer_name" class="col-md-10" placeholder="ชื่อกิจกรรม" style="border-radius: 25px;" value=""/ required> 
        </div><br>
        <div class="row g-3">
        <label for="location" class="col-md-2 col-form-label" align="left"><b>สถานที่</b></label>
        <input type="text" name="location" id="location" class="col-md-10" style="border-radius: 25px;" placeholder="สถานที่" value=""/ required> 
        </div>
        <br>
        <div class="row g-3">
        <label for="amphur" class="col-md-2 col-form-label"align="left"><b>อำเภอ</b></label>
        <input type="text" name="amphur" id="amphur" placeholder=อำเภอ style="border-radius: 25px;" value=""/ required>
        <label for="distinct" class="col-md col-form-label" align="left"><b>ตำบล</b></label>
        <input type="text" name="distinct" id="distinct" placeholder="ตำบล" style="border-radius: 25px;"value=""/ required>
        <label for="province" class="col-sm col-form-label"><b>จังหวัด</b></label>
        <input type="text" name="province" id="province" placeholder="จังหวัด" style="border-radius: 25px;"value=""/ required>
        </div>
        <br>
        <div class="row g-3">
        <label for="date" class="col-md-2 col-form-label" align="left"><b>วันที่</b></label>
        <input type="date" name="date" id="date" placeholder="วันที่" style="border-radius: 25px;"value=""/ required>
        <div class="col-md-3"></div>
        <label for="semester" class="col-md col-form-label" align="left"><b>ภาคการศึกษา</b></label>
        <input type="text" name="semester" id="semester" placeholder="ภาคการศึกษา" style="border-radius: 25px;"value=""/ required>
        </div><br>
        <div class="row g-3">
        <label for="photo" class="col-md col-form-label" align="left"><b>แนบไฟล์รูป</b></label>
        <div class="col-md-10">
        <input type="file" name="photo" id="photo"placeholder="ไฟล์รูป" style="border-radius: 25px;" value=""/ required></div>
        </div><br>
        <div class="row g-3">
        <div class="col-md-12">
        <label for="Hours"  align="center"><b>จำนวนชั่วโมง</b></label>
        <!--<input type="text" class="col-md-3 col-form-label" name="Hours" id="Hours" placeholder="จำนวนชั่วโมง" style="border-radius: 25px;"align="right" value=""/ required>-->
        <select name="Hours" id="Hours" >
                                    <option></option>
                                    <option value=1>1</option>
                                    <option value=2>2</option>
                                    <option value=3>3</option>
                                    <option value=4>4</option>
                                    <option value=5>5</option>
                                    <option value=6>6</option>
                                    <option value=7>7</option>
                                    <option value=8>8</option>
                                    <option value=9>9</option>
                                    <option value=10>10</option>
                                    <option value=11>11</option>
                                    <option value=12>12</option>
                            </select>
        </div>
    </div><br>
        <div class="row g-3">
        <label for="witness" class="col-md col-form-label" align="left"><b>ชื่อผู้รับรอง</b></label>
        <div class="col-md-6">
        <input type="text" class="form-control" name="witness" id="witness" placeholder="ชื่อผู้รับรอง" style="border-radius: 25px;"value=""/ required>
        </div>
        <div class="col-md-"></div>
        <br>
        <div class="row g-3">
        <label for="email" class="col-md-4 col-form-label" align="left"><b>emailผู้รับรอง</b></label>
        <div class="col-md-">
        <input type="text" class="form-control" name="email" id="email"placeholder="email ผู้รับรอง" style="border-radius: 25px;" value=""/ required>
        </div><br>
    </div>
        </div>
        <br>
        <div class="row g-3">
        <label for="description" class="col-md-2 col-form-label" align="left"><b>รายละเอียด</b></label>
        <div class="col-md-10">
        <textarea class="form-control" id="description" placeholder="รายละเอียด" required></textarea>
    </div>
    </div>
        <div class="row">
        <input type="hidden" name="student_id" id="hiddenField" value="<?php echo $student_id; ?>" />
        <div>
          <button type="submit"  name="upload" align="center">ยืนยัน</button>
        </div>
        </div>
    </form></div>
    <div class="col-sm-3"></div>
    </div>
    </div>
<?php } ?>
</body>
</html>