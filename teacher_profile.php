<?php 
header('Content-Type: text/html; charset=utf-8');
session_start(); ?>
<!DOCTYPE html>

<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}

</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php
	require('connect.php');
	if($_SESSION["teacher_id"]==""){
		header("Location: teacherlogin.php");
	}else{$teacher_id = $_SESSION["teacher_id"];
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
        <p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:#006400">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
		<div class="col-sm" align="center"><p><a href=teacher_index.php>หน้าหลัก</a></p></div>
		<div class="col-sm" align="center"><p><a href=teacher_volunteerapprove.php>การอนุมัติจิตอาสา</a></p></div>
        <div class="col-sm" align="center"><p style="color:brown"><b><u>ข้อมูลส่วนตัว</u></b></p></div>
    <div class="col-sm" align="center"><p><a href=logout.php name="logout" value="logout" id="logout">ออกจากระบบ</a></p></</div>
	</div>
</div>
</div>
</nav>
		<?php
		$query = "SELECT * FROM Teacher WHERE teacher_id ='$teacher_id'";
		$result = mysqli_query($conn,$query);
		$row = mysqli_fetch_assoc($result);
		//$classroom = $row['classroom'];
		?>
<div class="content">
    
<div class="row">
        <div class="col-sm-3 " align="center"></div>
        <div class="col-sm-2" align="center"><img src="<?php echo $row['photo'];?>" class="rounded-circle avatar";" style="width:200px; height:200px "  alt="Image"></div>
        <div class="col-sm-4 " align="left">
        <?php 
		 $query = "SELECT * FROM Teacher INNER JOIN Room ON Teacher.teacher_id = Room.teacher_id WHERE Teacher.teacher_id = '$teacher_id'";
		 $result = mysqli_query($conn,$query);
        $row = mysqli_fetch_assoc($result);
        //tel = $row['stel'];
        echo "<br>";
        echo '<p style="" align="left"><b><u>ข้อมูลส่วนตัว</u><b></p>
        <p>';echo 'ชื่อ '.$row['prefix'].' '.$row['firstname'].' '.$row['lastname'];echo'</p>
        <p>';echo 'รหัสครู '.$row['teacher_id'];echo'</p>
        <p>';echo 'เบอร์โทรศัพท์ '.$row['tel'];echo'</p>
        <p>';echo 'ห้องที่ดูแล '.$row['classroom']; echo'</p>
		<p>';?><button class="btn btn-info" onclick="window.location.href='teacher_updatetel.php'">แก้ไขข้อมูลส่วนตัว</button>
		<?php echo'</p>
        <p></p>';
		$classroom =$row['classroom'];
        ?>
        </div>
        <div class="col-sm-3 " align="center"></div>
        </div>
<br>
<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
<table class="table table-bordered"style="text-align:center" align="center">
  <thead>
    <tr class="table-success">
        <td scope="col" >ที่</td>
        <td scope="col">ชื่อ นามสกุล</td>
        <td scope="col">รหัสนักเรียน</td>
        <td scope="col">เบอร์โทรศัพท์</td>
    </tr>
  </thead><?php
        $query2 ="SELECT * FROM Student WHERE classroom = '$classroom'";
        $result = mysqli_query($conn,$query2);
        $count =1;
        $hour=0;
	?>
	<?php
    while($row2=mysqli_fetch_assoc($result)){
        echo'<tbody>
            <tr class="table-light">
                <td>';echo $count++;echo'
                <td>';echo $row2['prefix'].' ';echo $row2['firstname'].' ';echo $row2['lastname'];echo'</td>
                <td>';echo $row2['student_id'];echo'</td>
                <td>';echo $row2['tel'];echo'</td>
            </tr>
        </tbody>';
        }
	?>
</table>
</div>
<div class="com-sm-2"></div>
<?php } ?>
</body>
</html>