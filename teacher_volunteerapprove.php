<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}

</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle2.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<?php
	require('connect.php');
	if($_SESSION["teacher_id"]==""){
		header("Location: teacherlogin.php");
	}else{
    $teacher_id = $_SESSION["teacher_id"];
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
        <p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:#006400">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
		<div class="col-sm" align="center"><p><a href=teacher_index.php>หน้าหลัก</a></p></div>
        <div class="col-sm" align="center"><p style="color:brown"><b><u>การอนุมัติจิตอาสา</u></b></p></div>
        <div class="col-sm" align="center"><p><a href=teacher_profile.php>ข้อมูลส่วนตัว</a></p></div>
    <div class="col-sm" align="center"><p><a href=logout.php name="logout" value="logout" id="logout">ออกจากระบบ</a></p></</div>
	</div>
</div>
</div>
</nav>
<br>
<div class="content">
    <div class="row">
		<div class="col-sm-3" style="text-align:center">
			คำร้องขอชั่วโมงจิตอาสา
		</div>
		<div class="col-sm-9"></div>
	</div>
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm-8">
			<?php
			$query ="SELECT * FROM Volunteer JOIN Student On Volunteer.student_id = Student.student_id WHERE teacher_id ='$teacher_id'";
			$result = mysqli_query($conn,$query);
			$count=1;
				
				?>
<table class="table table-bordered"style="text-align:center" align="center">
	<thead>
		<tr class="table-success">
			<td scope="col" >ที่</td>
			<td scope="col">วันที่ยื่นคำขอ</td>
			<td scope="col">ชื่อ นามสกุล</td>
			<td scope="col">รหัสนักเรียน</td>
			<td scope="col">สถานที่</td>
			<td scope="col">จำนวนชั่วโมง</td>
			<td scope="col">อนุมัติ</td>
			<td scope="col">ดูรายละเอียด</td>
		</tr>
	</thead>
		<?php
			//$mysqli -> set_charset("utf8");
			$status = 'รอดำเนินการ';
			$query ="SELECT * FROM Volunteer JOIN Student On Volunteer.student_id = Student.student_id WHERE teacher_id ='$teacher_id' AND isapprove='รอดำเนินการ'";
			$result = mysqli_query($conn,$query);
			$count=1;
			while($row=mysqli_fetch_assoc($result)){
				$volunteer_id =$row['volunteer_id'];
			echo'<tbody>
				<tr class="table-light">
					<td>';echo $count++;echo'</td>
					<td>';echo $row['create_at'];echo'</td>
					<td>';echo $row['prefix'].' ';echo $row['firstname'].' ';echo $row['lastname'];echo'</td>
					<td>';echo $row['student_id'];echo'</td>
					<td>';echo $row['location'];echo'</td>
					<td>';echo $row['Hours'];echo'</td>
					<td><input type="button" class="btn btn-success" value="อนุมัติ" placeholder="approve" onclick="approve(\''.$volunteer_id.'\')">
					<input type="button"  class="btn btn-danger"value="ไม่อนุมัติ" onclick="reject(\''.$volunteer_id.'\')"></td>
					<td> <input type="button"  class="btn btn-info"value="ดูรายละเอียด" onclick="info(\''.$volunteer_id.'\')"></td>
				</tr>
			</tbody>';
		}
	?>
</table>
				<?php
				$count++;
			?>
		</div>
		<div class="col-sm-2"></div>
	</div>
    </div>

	<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css" rel="stylesheet" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<script type="text/javascript">
function dosomething(val){


	alert(<?php echo $volunteer_id;?>)
	//alert(val);
}
function approve(val){
	var i=0;
	window.location.href = "approve.php?volunteer=" + val; 
	
}
function info(val){
	//alert (val);
	window.location.href = "info.php?volunteer=" + val;
}
function reject(val){
	//alert(val);
	const volunteer_id = val;
	//alert (volunteer_id);
	swal({
                title: "เหตุผล",
                text: "ระบุเหตุผลที่ไม่อนุมัติ:",
                type: "input",
                backdrop: false,
                showCancelButton: false,
                closeOnConfirm:false,
                animation: "slide-from-top",
                inputPlaceholder: "เขียนเหตุผล",
                },function(inputValue){
                    swal("Nice!", "ระบุเหตุผลเสร็จสิ้น: " + inputValue, "success");
					window.location.href = "reject.php?volunteer="+volunteer_id+"&annotate="+inputValue; 
                    setTimeout("window.location.href = 'teacher_volunteerapprove.php';",3000);
                }); 
	//window.location.href = "reject.php?volunteer="+volunteer_id+"&annotate="+inputValue; 
}

	</script>
	<?php } ?>
</body>
</html>