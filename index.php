<?php session_start(); ?>
<!DOCTYPE html>
<html>
<head>
</head>
<style>
.sticky {
  position: fixed;
  top: 0;
  width: 100%;
}
.p{
	font-family: "kanit", "Arial", sans-serif;
}
.sticky + .content {
  padding-top: 102px;
}
.header {
  	padding: 10px 16px;
	color: #000000;
  	background: #ffffff;
}
</style>
<body>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<link href="https://fonts.googleapis.com/css2?family=Dancing+Script:wght@500&display=swap" rel="stylesheet">
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<link rel="stylesheet" href="css/mystyle.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type=text/javascript src="jquery.min.js"></script>
<script src="jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
<?php
	require('connect.php');
	if($_SESSION["student_id"]==""){
		header("Location: studentlogin.php");
	}else{
		$student_id=$_SESSION["student_id"];
		if (isset($_POST['liked'])) {
			$announce_id = $_POST['announce_id'];
			mysqli_query($conn, "INSERT INTO liked (student_id, announce_id) VALUES ('$student_id', '$announce_id')");
			exit();
		}
		if (isset($_POST['unliked'])) {
			$announce_id = $_POST['announce_id'];
			mysqli_query($conn, "DELETE FROM liked WHERE announce_id='$announce_id' AND student_id='$student_id'");
			exit();
		}
	
?>
<nav id="navbar_top">
<div class="header" id="myHeader">
<div class="container">
	<div class="row">
		<div class="col-sm"></div>
		<div class="col-sm-6;">
		<span class="border-bottom">
				<p style="text-align:center; font-size:440% ;font-family:Trebuchet MS, sans-serif ;color:pink">JITARSA</p>
		</span>
		</div>
		<div class="col-sm"></div>
	</div>
	<div class="row">
		<div class="col-sm" align="center"><p style="color:brown"><b><u>หน้าหลัก</u></b></p></div>
		<div class="col-sm" align="center"><p><a href=stu_volunteerinfo.php>ข้อมูลจิตอาสา</a></p></div>
		<div class="col-sm" align="center"><p><a href=stu_profile.php>ข้อมูลส่วนตัว</a></p></div>
		<div class="col-sm" align="center"><p><a href=logout.php name="logout" value="logout" id="logout">ออกจากระบบ</a></p></div>
	</div>
</div>
</div>
</nav>

<div class="content">
	<div class="row">
		<div class="col-sm-2"></div>
		<div class="col-sm">
			<?php
			$query = "SELECT *,Announce.photo as ap,CAST(Announce.announce_id AS UNSIGNED) AS id From announce INNER JOIN Teacher ON announce.teacher_id = teacher.teacher_id ORDER BY id DESC";
			$result = mysqli_query($conn,$query);
			while($row =mysqli_fetch_assoc($result)){
				$query2 = "SELECT COUNT(*) as total FROM liked WHERE announce_id ='$row[announce_id]'";
				$data = mysqli_query($conn,$query2);
				$ans = mysqli_fetch_assoc($data); //count like of announce?>
				<div class="post"><?php
				$results = mysqli_query($conn, "SELECT * FROM liked WHERE student_id='$student_id' AND announce_id=".$row['announce_id']."");
				if (mysqli_num_rows($results) == 1 ): 
				echo '<br>';
				echo '<div class="panel panel-danger">
					<div class="panel-heading">';echo 'ชื่อกิจกรรม '.$row['announce_name'];echo '</div>
					<div class="panel-body"><img src="';echo $row['ap'];echo '" class="img-thumbnail;" style="width:20%;" alt="Image"></div>
					<div class="panel-body">';echo 'รายละเอียด: '.$row['announce_description']; echo '</div>
					<div class="panel-body">';echo 'สถานที่: '.$row['location']; echo '</div>
					<div class="panel-footer">';?>
					<span><a href="" class="unlike" data-id="<?php echo $row['announce_id'];?>">ถูกใจแล้ว</a></span>
					<span><a href="" class="hide like" data-id="<?php echo $row['announce_id'];?>">ถูกใจ</a></span>
				<span class="likes_count"><?php echo $ans['total']; ?> คนถูกใจสิ่งนี้</span></div></div>
				<?php else: 
				echo '<br>';
				echo '<div class="panel panel-danger">
					<div class="panel-heading">';echo 'ชื่อกิจกรรม '.$row['announce_name'];echo '</div>
					<div class="panel-body"><img src="';echo $row['ap'];echo '" class="img-thumbnail;" style="width:20%;" alt="Image"></div>
					<div class="panel-body">';echo 'รายละเอียด: '.$row['announce_description']; echo '</div>
					<div class="panel-body">';echo 'สถานที่: '.$row['location']; echo '</div>
					<div class="panel-footer">';?>
					<span><a href="" class="like" data-id="<?php echo $row['announce_id'];?>">ถูกใจ</a></span>
					<span><a href="" class="unlike hide" data-id="<?php echo $row['announce_id'];?>">ถูกใจแล้ว</a></span>
				<span class="likes_count"><?php echo $ans['total']; ?> คนถูกใจสิ่งนี้</span></div></div>
				<?php endif;?>
				</div><?php
			  }
			?>
			</div>
		<div class="col-sm-2"></div>
	</div>	
</div>
<?php } ?>
<script type="text/javascript"  
    src="http://code.jquery.com/jquery.min.js" 
    src="jquery.min.js"
    src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"
></script>
<script type="text/javascript" src="jquery.min.js"></script>

<script>
$(document).ready(function(){
		$('.like').on('click', function(){
			var announce_id = $(this).data('id');
				alert('you click liked');
			    $post = $(this);
			$.ajax({
				url: 'index.php',
				type: 'post',
				data: {
					'liked':1,
					'announce_id': announce_id
				},
				success: function(response){
					$post.parent().find('span.likes_count').text(response + " likes");
					$post.addClass('hide');
					$post.siblings().removeClass('hide');
				}
			});
		});
		$('.unlike').on('click', function(){
			var announce_id = $(this).data('id');
				alert ('you unlike');
		    	$post = $(this);
			$.ajax({
				url: 'index.php',
				type: 'post',
				data: {
					'unliked': 1,
					'announce_id': announce_id
				},
				success: function(response){
					$post.parent().find('span.likes_count').text(response + " likes");
					$post.addClass('hide');
					$post.siblings().removeClass('hide');
				}
			});
		});
	});
document.addEventListener("DOMContentLoaded", function(){
  window.addEventListener('scroll', function() {
      if (window.scrollY > 50) {
        document.getElementById('navbar_top').classList.add('fixed-top');
        // add padding top to show content behind navbar
        navbar_height = document.querySelector('.navbar').offsetHeight;
        document.body.style.paddingTop = navbar_height + 'px';
      } else {
        document.getElementById('navbar_top').classList.remove('fixed-top');
         // remove padding top from body
        document.body.style.paddingTop = '0';
      } 
  });
}); 
</script>
</body>
</html>